package com.cts.patient;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.cts.patient.config.AppConfig;
import com.cts.patient.service.PatientService;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	AnnotationConfigApplicationContext cont = new AnnotationConfigApplicationContext(AppConfig.class);
    	PatientService bean = (PatientService)cont.getBean(PatientService.class);
    	bean.getPatientDetails();
    }
}
