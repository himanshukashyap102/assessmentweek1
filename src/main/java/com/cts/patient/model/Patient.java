package com.cts.patient.model;

//import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class Patient {
//	@Value("${pid}")
	private String pid;
//	@Value("${pname}")
	private String pname;
//	@Value("${disease}")
	private String disease;
//	@Value("${sex}")
	private String sex;
//	@Value("${admit_status}")
	private String admit_status;
//	@Value("${age}")
	private String age;
	
	public String getPid() {
		return pid;
	}
	public String getPname() {
		return pname;
	}
	public String getDisease() {
		return disease;
	}
	public String getSex() {
		return sex;
	}
	public String getAdmit_status() {
		return admit_status;
	}
	public String getAge() {
		return age;
	}
	public void setPid(String pid) {
		this.pid = pid;
	}
	public void setPname(String pname) {
		this.pname = pname;
	}
	public void setDisease(String disease) {
		this.disease = disease;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public void setAdmit_status(String admit_status) {
		this.admit_status = admit_status;
	}
	public void setAge(String age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Patient [pid=" + pid + ", pname=" + pname + ", disease=" + disease + ", sex=" + sex + ", admit_status="
				+ admit_status + ", age=" + age + "]";
	}
	
	
}
