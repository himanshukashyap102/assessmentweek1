package com.cts.patient.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.cts.patient.model.Patient;
import com.cts.patient.service.PatientService;

@Configuration
@ComponentScan("com.cts.patient.service.PatientService")
@PropertySource("patient.properties")
public class AppConfig {
	@Autowired
	Environment env;
	
	@Bean	
	public PatientService beanP() {
		Patient patient= new Patient();
		patient.setPid(env.getProperty("pid"));
		patient.setPname(env.getProperty("pname"));
		patient.setAge( env.getProperty("age"));
		patient.setSex(env.getProperty("sex"));
		patient.setAdmit_status(env.getProperty("admit_status"));
		patient.setDisease(env.getProperty("disease"));	
		return new PatientService(patient);
		}
	}

