package com.cts.patient.service;
import org.springframework.stereotype.Component;

import com.cts.patient.model.Patient;
@Component

public class PatientService {
	private Patient patient;
	public PatientService(Patient patient) {
		this.patient=patient;
	}
	
	public void getPatientDetails() {
		System.out.println(this.patient);
	}
}
